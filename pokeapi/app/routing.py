# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""Application internal routing utilities."""

import os
from importlib import import_module
from pathlib import Path

from fastapi import APIRouter
from fastapi import FastAPI

from ..api.routing import build_prefix
from ..conf import settings

try:
    from fastapi.staticfiles import StaticFiles
except ImportError:
    # Prod doesn't have this package and it's ok, it doesn't use it
    StaticFiles = None


def find_and_add_apis(application: FastAPI) -> None:
    """Find API endpoints and add their router."""
    api_path = Path(os.path.join(settings.BASE_DIR, 'api'))
    for version_path in sorted(api_path.glob('v*/'), key=lambda p: p.name):
        if version_path.is_dir():
            module_urls = import_module(
                f'.api.{version_path.name}.urls',
                settings.PACKAGE_NAME,
            )
            api_router = getattr(module_urls, 'api_router')
            version_prefix = getattr(module_urls, 'version_prefix')
            application.include_router(api_router, prefix=build_prefix(version_prefix))


def _get_static_files(*args, **kwargs) -> 'StaticFiles':
    try:
        return StaticFiles(*args, **kwargs)
    except TypeError:
        raise ImportError('aiofiles is not installed, run `poetry add --dev aiofiles`')


def configure_app_routers(app: FastAPI, root_router: APIRouter) -> None:
    """Configure application routers."""
    # Add root API
    app.include_router(root_router, prefix=build_prefix(''))

    # Find and add API endpoints
    find_and_add_apis(app)

    if settings.DEVELOPMENT_MODE:
        # Mount static assets route
        static_dir = os.path.join(settings.BASE_DIR, 'static')
        static_files = _get_static_files(directory=static_dir)
        app.mount(
            build_prefix(settings.STATIC_URL_PATH),
            static_files,
            name='static',
        )
