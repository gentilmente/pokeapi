"""App exception handlers."""

from fastapi import Request
from fastapi import status
from fastapi.responses import JSONResponse

from .asgi import app
from ..exceptions import ObjectNotFound
from ..exceptions import ValidationError


@app.exception_handler(ValidationError)
async def validation_error_exception_handler(
        request: Request,
        exc: ValidationError,
) -> JSONResponse:
    """Handle ValidationError exceptions properly."""
    return JSONResponse(
        status_code=status.HTTP_400_BAD_REQUEST,
        content={'detail': f'{exc}'},
    )


@app.exception_handler(ObjectNotFound)
async def object_not_found_exception_handler(
        request: Request,
        exc: ObjectNotFound,
) -> JSONResponse:
    """Handle ObjectNotFound exceptions properly."""
    return JSONResponse(
        status_code=status.HTTP_404_NOT_FOUND,
        content={'detail': f'{exc}'},
    )
