# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""Expose application settings."""

from .configs import Settings
from .configs import get_settings
from .lazy import LazySettings

# ToDo: Type hint this correctly.
# noinspection PyTypeChecker
settings: Settings = LazySettings()

__all__ = (
    'Settings',
    'settings',
    'get_settings',
)
