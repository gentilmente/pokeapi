"""Trainer CRUD."""

from .base import Crud
from .. import models


class Trainer(Crud):
    """Trainer CRUD."""

    model = models.Trainer
