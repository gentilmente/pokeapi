# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""Trainer selectors."""

import typing

from pokeapi import crud
from pokeapi import exceptions
from pokeapi import models
from pokeapi.db import SessionLocal


async def get_trainer(
        *,
        db: SessionLocal,
        id: int,  # noqa: A002
) -> models.Trainer:
    """Get a trainer.

    :raise ObjectNotFound: Trainer not found.
    """
    trainer: typing.Optional[models.Trainer] = await crud.Trainer(db).read(
        one_or_none=True,
        filters=[
            models.Trainer.id == id,
        ],
    )
    if not trainer:
        raise exceptions.ObjectNotFound('trainer not found')

    return trainer


async def get_trainers(
        *,
        db: SessionLocal,
        offset: int = 0,
        limit: int = 20,
) -> typing.List[models.Trainer]:
    """Get all trainers."""
    trainers: typing.List[models.Trainer] = await crud.Trainer(db).read(
        offset=offset,
        limit=limit,
    )
    return trainers
